program fibonaccigame;
uses crt;

function fibonacci(n: integer): longint;
var
    i, p, q, r: longint;
begin
    if (n = 0) or (n = 1) then
    begin
        fibonacci := n;
        exit;
    end;
    q := 0;
    r := 1;
    for i := 2 to n do
    begin
        p := q;
        q := r;
        r := p + q;
    end;
    fibonacci := r;
end;

procedure gameOver(stage: integer);
var
    i: integer;
begin
    clrscr;
    GotoXY(1, ScreenHeight div 2 - 1);
    for i := 0 to stage do
        write(fibonacci(i), ' ');
    delay(3000);
    clrscr;
    halt(0);
end;

function SrI(s: string): longint;
var
    i: integer;
    res: longint;
    negative: boolean;
begin
    res := 0;
    negative := false;
    for i := 1 to length(s) do
    begin
        if s[1] = '-' then
            negative := true
        else
        begin
            res *= 10;
            res += ord(s[i]) - ord('0');
        end;
    end;
    if negative then
        res *= -1;
    SrI := res;
end;

procedure readans(var n: longint);
var
    ch: char;
begin
    GotoXY(1, ScreenHeight div 2);
    write('Your answer: ');
    n := 0;
    while true do
    begin
        ch := ReadKey;
        case ch of
            #13: break;
            #27:
            begin
                n := -123;   { Code to over game }
                exit;
            end; 
            #8:
            begin
                write(#8' '#8);
                n := n div 10;
            end;
        end;
        if (ch >= #48) and (ch <= #57) then
        begin
            write(ch);
            n *= 10;
            n += ord(ch) - ord('0'); 
        end;
    end;
end;

var
    stage: integer;
    n, rans: longint;
begin
    stage := 2;
    while true do
    begin
        clrscr;
        GotoXY(1, ScreenHeight div 2 - 1);
        write(fibonacci(stage-2), ' ', fibonacci(stage-1), ' ?');
        rans := fibonacci(stage);
        readans(n);
        if n = rans then
            stage += 1
        else if n = -123 then
            gameOver(stage-1)
        else
        begin
            GotoXY(1, ScreenHeight div 2 + 1);
            write('You lost, right answer: ', rans);
            delay(2000);
            gameOver(stage-1);
        end;
    end;
end.
